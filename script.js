function randomPalette(){
    var palette = createPallete();
    setPalette(palette);
}

function resetPallete() {
    setPalette([[255,255,255],[255,255,255],[255,255,255],[255,255,255],[255,255,255]])
}

function setPalette(palette) {
    $('.color-view').each(function(i) {
        var rgbValue = rgbToCSS(palette[i])
        $(this).css('background-color', rgbValue)
    })
    generateRules(palette);
}

function generateRules(palette){
    text = '';
    text += `.website-background{ color: ${rgbToCSS(palette[0])};}\n\n`
    text += `.element-text{ color: ${rgbToCSS(palette[1])};}\n\n`
    text += `.element-border{ border-color: ${rgbToCSS(palette[2])};}\n\n`
    text += `.element-background{ background-color: ${rgbToCSS(palette[3])};}\n\n`
    text += `.header{ color: ${rgbToCSS(palette[4])};}\n\n`

    $('#css-rules').val(text);

}

function createPallete() {
    var initialHue = Math.floor(Math.random()*360);
    var colors = []

    for (let i = 0; i < 5; i++) {
        var displacement = (initialHue + i*72) % 360;
        colors.push(roundRgb(hsvToRgb(displacement/360, 1, 1)));        
    }

    return colors;
}

function roundRgb(rgb) {
    return rgb.map(value => Math.round(value))
}

function rgbToCSS(rgb) {
    return `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`
}